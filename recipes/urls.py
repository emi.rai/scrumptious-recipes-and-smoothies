from django.urls import path
from .views import show_recipe, recipe_list, create_recipe, edit_recipe,  my_recipe_list


urlpatterns = [
    path("<int:id>/", show_recipe, name="show_recipe"),
    path("", recipe_list, name="recipe_list"),
    path("list/", recipe_list, name="recipe_list"),
    path("create/", create_recipe, name="create_recipe"),
    path("<int:id>/edit/", edit_recipe, name="edit_recipe"),
    path("mine/", my_recipe_list, name="my_recipe_list"),
]



    # The below is not best practice because you would have to change all 
    # of the recipe tabs if you updated anthing. Best practice is to have an 
    # easily changeable paths in urls.py in the project
    # path("/", include("recipes.urls")), in the urls.py in the project?
    # path("recipes/<int:id>/", show_recipe, name="show_recipe"),
    # path("recipes/list", recipe_list, name="recipe_list"),